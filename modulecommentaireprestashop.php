<?php


require_once 'App/Controllers/Hook/displayProductTabContentController.php';
require_once _PS_MODULE_DIR_ . 'modulecommentaireprestashop/App/Models/displayProductTabContentModel.php';


/*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA

*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/
// On démarre toujours avec un test de version

if (!defined('_PS_VERSION_')) {
    exit;
}
/**
 * Class ModuleCommentairePrestashop
 */
class ModuleCommentairePrestashop extends Module
{
    /**
     * @var bool
     */
    protected $config_form = false;
    /**
     * ModuleCommentairePrestashop constructor.
     */
    public function __construct()
    {
        // Nom du module dans le système Prestashop (obligatoire)
        $this->name = 'modulecommentaireprestashop';
        // Nom visible par l'utilisateur dans le BO (obligatoire)
        $this->displayName = 'Module de commentaire sur les fiches produits';
        // Catégorie du module pour la recherche de module dans le BO (optionnel)
        $this->tab = 'administration';
        // Version du module (optionnel)
        $this->version = '0.1.0';
        // Nom de l'auteur (optionnel)
        $this->author = 'Jérôme Diaferia';

        // Rappel du parent
        parent::__construct();
        // Description (optionnel)
        $this->description = 'Module de commentaire de sur les fiches produits avec notation';
    }

    public function install()
    {
        parent::install();

        // Ici nous demandons à notre module de se greffer à la position 'displayProductTabContent'
        $this->registerHook('displayProductTabContent');
        // Pour afficher les commentaires sur la page produit
        $this->registerHook('displayComments');

        // Import des tables SQL
        $this->loadDb();
        
        return true;
    }

    public function uninstall()
    {
        parent::uninstall();

        // Suppression des tables SQL
        $this->loadDb('drop');

        return true;
    }

    /**
     * Permet d'ajouter une page d'administration
     * @return string
     */
    public function getContent()
    {
        $html_confirmation_message = $this->display(__FILE__, 'getContent.tpl');
        $html_form = $this->formContent();

        return $html_confirmation_message.$html_form;
    }

    public function formContent()
    {
        $fields_form = [
          'form' => [
              'legend' => [
                  'title' => 'Configuration du module'
              ],
              'input' => [
                  [
                      'type' => 'switch',
                      'label' => 'Activer commentaire ?',
                      'name' => 'commentaire_active',
                      'values' => [
                          [
                              'id' => 'commentaire_active_1',
                              'value' => 1,
                              'label' => 'Activé'
                          ],
                          [
                              'id' => 'commentaire_active_',
                              'value' => 0,
                              'label' => 'Désactivé'
                          ]
                      ]
                  ]
              ],
              'submit' => [
                  'title' => 'Sauvegarder'
              ]
          ]
        ];

        $helper = new HelperForm();
        $helper->table = 'modcompresta';
        $helper->submit_action = 'submit_btn_modcompresta';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) .
            '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = [
            'fields_value' => [
                'commentaire_active' => Tools::getValue('commentaire_active' , Configuration::get('MODCOMPRESTA'))
            ]
        ];
        return $helper->generateForm([$fields_form]);
    }

    public function hookDisplayProductTabContent($params)
    {

        // Ici j'importe la classe qui sera chargée du controle et de l'affichage de la vue
//        require_once 'App/Controllers/Hook/displayProductTabContentController.php';
        $controller = new DisplayProductTabContentController($this, __FILE__, $this->_path);
        $controller->processProduct();

        return $this->display(__FILE__, 'displayProductTabContent.tpl');

    }

    public function hookDisplayComments($params)
    {
        $id_product = Tools::getValue('id_product');
        $comments = DisplayProductTabContentModel::getProductComments($id_product, 10);

        $this->context->smarty->assign('comments', $comments);

        return $this->display(__FILE__, '../front/comments.tpl');

    }

    public function loadDb($params = 'create')
    {
        $return = true;
        include(__DIR__.'/install/install.php');
        // Si je demande la suppression je boucle sur les noms de table
        // Sinon je créé des tables
        foreach ($sql as $name => $s) {
            if($params === 'drop'){
                Db::getInstance()->execute('DROP TABLE '.$name);
            }else{
                $return &= Db::getInstance()->execute($s);
            }
        }
        return $return;
    }

}