<h3>Laissez un commentaire :</h3>
{if isset($new_comment_posted) }
    <div class="alert alert-success" role="alert">
        {$new_comment_posted}
    </div>
{/if}


<div class="row">
    <div class="col-12">
        <form id="rating" method="post" action="">
            <div class="form-group">
                <label for="email">Email address</label>
                <input type="email" name="email" class="form-control" id="email" placeholder="name@example.com*" required>
            </div>
            <div class="form-group">
                <label for="firstname">Prénom</label>
                <input type="text" name="firstname" class="form-control" id="firstname" placeholder="Prénom*" required>
            </div>
            <div class="form-group">
                <label for="lastname">Nom</label>
                <input type="text" name="lastname" class="form-control" id="lastname" placeholder="Nom*" required>
            </div>
            <div class="form-group">
                <label for="note">Note :</label>
                <select class="form-control" id="note" name="rating">
                    <option value="0">Choisir une note</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                </select>
            </div>
            <div class="form-group">
                <label for="comment">Commentaire :</label>
                <textarea name="comment" id="comment" cols="30" rows="10" class="form-control"></textarea>
            </div>
            <div class="form-group">
                <button class="btn-primary" name="new_comment">Envoyer</button>
            </div>
        </form>
    </div>
</div>