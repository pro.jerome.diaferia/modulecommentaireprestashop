<div class="row">
    {foreach from=$comments item=comment}
        <div class="col-12 bg-warning">
            <blockquote class="blockquote text-center">
                <button type="button" class="btn btn-primary">
                    Note <span class="badge badge-light">{$comment.rating} / 5</span>
                </button>
                <p class="mb-0">Commentaire #{$comment.id_modcompresta_comment} : {$comment.comment}</p>
                <footer class="blockquote-footer">{$comment.firstname} {$comment.lastname|substr:0:1}
                    <cite title="Source Title">{$comment.date_add|substr:0:10}</cite>
                </footer>
            </blockquote>
        </div>
    {/foreach}
</div>