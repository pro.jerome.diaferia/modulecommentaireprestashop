<?php
/*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA

*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

require_once _PS_MODULE_DIR_ . 'modulecommentaireprestashop/App/Models/displayProductTabContentModel.php';

class DisplayProductTabContentController
{
    public function __construct($module, $file, $path)
    {
        $this->file = $file;
        $this->module = $module;
        $this->context = Context::getContext();
        $this->_path = $path;
    }

    // Nous allons créer un constructeur chargé de récupérer les données

    public function processProduct()
    {

        if (Tools::isSubmit('new_comment')) {
            $id_product = Tools::getValue('id_product');
            $firstname = Tools::getValue('firstname');
            $lastname = Tools::getValue('lastname');
            $email = Tools::getValue('email');
            $rating = Tools::getValue('rating');
            $comment = Tools::getValue('comment');


            if (!Validate::isName($firstname) ||
                !Validate::isName($lastname) ||
                !Validate::isEmail($email) ||
                !Validate::isInt($rating) ||
                !Validate::isString($comment)) {

                $this->context->smarty->assign('new_comment_posted', 'error');
                return false;

            }


            $insert_comment = new DisplayProductTabContentModel();
            $insert_comment->id_product = (int)$id_product;
            $insert_comment->firstname = $firstname;
            $insert_comment->lastname = $lastname;
            $insert_comment->email = $email;
            $insert_comment->rating = (int)$rating;
            $insert_comment->comment = nl2br($comment);
            $insert_comment->add();

            $this->context->smarty->assign('new_comment_posted', 'success');

        }
    }
}