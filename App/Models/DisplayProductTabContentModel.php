<?php
/*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA

*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class DisplayProductTabContentModel extends ObjectModel
{

    public $id_modcompresta_comment;
    public $id_product;
    public $product_name;
    public $firstname;
    public $lastname;
    public $email;
    public $rating;
    public $comment;
    public $date_add;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'modcompresta_comment', 'primary' => 'id_modcompresta_comment', 'multilang' => false,
        'fields' => array(
            'id_product' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
            'firstname' => array('type' => self::TYPE_STRING, 'validate' => 'isName', 'size' => 20),
            'lastname' => array('type' => self::TYPE_STRING, 'validate' => 'isName', 'size' => 20),
            'email' => array('type' => self::TYPE_STRING, 'validate' => 'isEmail'),
            'rating' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'comment' => array('type' => self::TYPE_HTML, 'validate' => 'isCleanHtml'),
            'date_add' => array('type' => self::TYPE_DATE, 'validate' => 'isDate', 'copy_post' => false),
        ),
    );

    public static function getProductComments($id_product, $limit)
    {
        $sql = new DbQuery();
        $sql->select('*');
        $sql->from('modcompresta_comment');
        $sql->where('id_product = '.$id_product);
        $sql->limit($limit);
        $sql->orderBy('date_add');

        return Db::getInstance()->executeS($sql);
    }

}